package com.oreillyauto.java.week2.classes;
public class MyClass {
    
    private int nonStaticVariable  = 0;
    
    public MyClass() {
        MyInnerClass mic = new MyInnerClass();
        System.out.println(mic.getInnerMethod());
    }
    
    public class MyInnerClass {
        protected final int getInnerMethod() {
            int increment = nonStaticVariable += 1;
            return increment;    
        }
    }
    
    public static void main(String[] args) {
        new MyClass();
    }
}
