package com.oreillyauto.java.week1.day01.classes;

public class MyClassHarness {

    public MyClassHarness() {
    }

    public static void main(String[] args) {
        MyClass test = new MyClass();
        
        String accessed = test.accessible;
         
        //This won't work compiler error( have to access from getter)
        String notAccessed = test.encapsulated;
    }
}
