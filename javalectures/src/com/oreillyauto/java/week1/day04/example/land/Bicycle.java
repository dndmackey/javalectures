package com.oreillyauto.java.week1.day04.example.land;

import com.oreillyauto.java.week1.day04.example.interfaces.NonMotorTransport;

public class Bicycle extends LandVehicle implements NonMotorTransport {

    public Bicycle() {
        System.out.println("Dude you got a byke!");
    }

    @Override
    public void move() {
        System.out.println("Moved to Harps");
    }

    @Override
    public String makeNoise() {
        return "we already had bread";
    }

   
}
