package com.oreillyauto.java.week1.day04.example.interfaces;

public interface MotorTransport {
    String vroom(); 
    Integer gallonsOf();
    String hornSound();
}
