package com.oreillyauto.java.week1.day04;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.oreillyauto.java.classes.AgeComparator;
import com.oreillyauto.java.classes.NameComparator;
import com.oreillyauto.java.classes.Ocean;
import com.oreillyauto.java.classes.Player;
import com.oreillyauto.java.classes.Student;
import com.oreillyauto.java.week1.day04.interfaces.MyInterfaceImpl;

public class W5D4TestHarness {

	public W5D4TestHarness() {
//		testAnimalTime();
//		testConstructors(); // set debug to 1 in Animal and Mammal
//		testMammalForAnimal(); // set debug to 0 in Animal and Mammal
//		testSuperclassLists();
//		extendingExample();
//		testInterfaces();
//		makeItRain();
//		testLinkedList();
//		testComparable();
//		testComparator();
	}

	private void testSuperclassLists() {
		Reptile reptile = new Reptile();
		Mammal mammal = new Mammal();
		Animal animal = new Animal();

		List<Animal> animalList = new ArrayList<Animal>();
		animalList.add(reptile);
		animalList.add(mammal);
		animalList.add(animal);

		for (Animal currentAnimal : animalList) {
			System.out.println(currentAnimal.animalTime());
		}
	}

	private void testMammalForAnimal() {
		Mammal mammal = new Mammal();
		animalAsArgument(mammal);
	}

	private void animalAsArgument(Animal animal) {
		System.out.println(animal.animalTime());

//        if (animal instanceof Animal) {
//            System.out.println("Animal passed in is instance of Animal.");
//        }
//        
//        if (animal instanceof Mammal) {
//            System.out.println("Animal passed in is instance of Mammal.");
//        }
	}

	private void testConstructors() {
		Mammal mammal = new Mammal("intern");
		System.out.println("mammal.getI():\t" + mammal.getI());
		System.out.println("mammal.getJ():\t" + mammal.getJ());
	}

	private void testAnimalTime() {
		Mammal mammal = new Mammal();
		System.out.println("mammal.animalTime():\t\t" + mammal.animalTime());
		System.out.println("mammal.invokeSuperTime():\t" + mammal.invokeSuperTime());
	}

	private void testComparator() {
		List<Student> al = new ArrayList<Student>();
		al.add(new Student(101, "Vijay", 23));
		al.add(new Student(106, "Jai", 27));
		al.add(new Student(105, "Ajay", 27));

		System.out.println("Sorting by Name...");

		Collections.sort(al, new NameComparator());
		
//		Collections.sort(al, new Comparator<Student>() {
//            @Override
//            public int compare(Student o1, Student o2) {
//                // TODO Auto-generated method stub
//                return 0;
//            }
//		});
		
		
		Iterator<Student> itr = al.iterator();

		while (itr.hasNext()) {
			Student st = (Student) itr.next();
			System.out.println(st.rollno + " " + st.name + " " + st.age);
		}

		System.out.println("sorting by age...");

		Collections.sort(al, new AgeComparator());
		Iterator<Student> itr2 = al.iterator();

		while (itr2.hasNext()) {
			Student st = (Student) itr2.next();
			System.out.println(st.rollno + " " + st.name + " " + st.age);
		}

	}

	private void testComparable() {
		Player player1 = new Player("Bob", 2);
		Player player2 = new Player("Sue", 1);
		LinkedList<Player> playerList = new LinkedList<Player>();
		playerList.add(player1);
		playerList.add(player2);
		Collections.sort(playerList);

		/** Example 1 */
		// Edit the compare function in Player
		// Ensure sort by rank is uncommented
		
		/** Example 2 */
		// Edit the compare function in Player
		// Ensure sort by name is uncommented
		
		for (Player player : playerList) {
			System.out.println(player);
		}
	}

	private void testLinkedList() {
		// Creating linkedList of class linked list
		LinkedList<String> linkedList = new LinkedList<String>();
		             
		// letters : D A E B C F B G 
		// position: 0 1 2 3 4 5 6 7 ?		
		// Adding elements to the linked list
		linkedList.add("A");      // queue
		linkedList.add("B");      // queue
		linkedList.addLast("C");  // add last!
		linkedList.addFirst("D"); // add first!
		linkedList.add(2, "E");   // insert into position [2]
		linkedList.add("F");      // queue
		linkedList.add("B");      // queue
		linkedList.add("G");      // queue 
//		System.out.println("Linked list : " + linkedList); // output?

		// letters : A E F B
		// position: 0 1 2 3 4 5 6 7
		// Removing elements from the linked list
		linkedList.remove("B");
		linkedList.remove(3);
		linkedList.removeFirst();
		linkedList.removeLast();
//		System.out.println("Linked list after deletion: " + linkedList); // ?

		// Finding elements in the linked list
//		boolean status = linkedList.contains("E");
//
//		if (status)
//			System.out.println("List contains the element \"E\" ");
//		else
//			System.out.println("List doesn't contain the element \"E\"");

		// Number of elements in the linked list
		int size = linkedList.size();
		System.out.println("Size of linked list = " + size);

		// A E F B
		// Get and set elements from linked list
		Object element = linkedList.get(2);
		System.out.println("Element returned by get() : " + element);
		linkedList.set(2, "Y");
		System.out.println("Linked list after change : " + linkedList);
	}

	private void makeItRain() {
		// Classes in this example: 
		//   MyAbstractWater.java (Abstract)
		//   Ocean.java (Concrete)
		//   Type.java (Enum) {"salty", "muddy", "instant"}
		
		// We cannot instantiate abstract classes.
//		MyAbstractWater maw = new MyAbstractWater();
		// Compilation error: MyAbstractWater cannot be resolved to a type
		
		// Ocean extends MyAbstractWater
		//   implements abstract method getType()
		// Gets the type of water found in the ocean...
		Ocean deadSea = new Ocean();
		System.out.println("type: " + deadSea.getType());
	}

	private void testInterfaces() {
		MyInterfaceImpl mii = new MyInterfaceImpl();
		System.out.println(mii.doSomethingElse("1", "2"));
		System.out.println(mii.doSomethingElse("1"));
	}

	private void extendingExample() {
		Dolphin d = new Dolphin();
		System.out.println("d.animalTime():\t\t" + d.animalTime());
		System.out.println("d.invokeSuperTime():\t" + d.invokeSuperTime());
	}

//    private void testConstructors() {
//        Mammal mammal = new Mammal("intern");
//        System.out.println("mammal.getI():\t" + mammal.getI());
//        System.out.println("mammal.getJ():\t" + mammal.getJ());
//    }

//    private void testMammalForAnimal() {
//        Mammal mammal = new Mammal();
//        animalAsArgument(mammal);
//    }
//    private void animalAsArgument(Animal animal) {
//        System.out.println(animal.animalTime());
//    }

//    private void animalAsArgument(Animal animal) {
//
//        if (animal instanceof Animal) {
//            System.out.println("Animal passed in is instance of Animal.");
//        }
//
//        if (animal instanceof Mammal) {
//            System.out.println("Animal passed in is instance of Mammal.");
//        }
//    }

	public static void main(String[] args) {
		new W5D4TestHarness();
	}
}
