package com.oreillyauto.java.week1.day03.classes;

import com.oreillyauto.java.week1.day03.pojos.Animal;

public class MyGenericClass3<T extends Animal> { 
    private T number;
    
    public void set(T object) { 
        this.number = object; 
    } 

    public T get() { 
        return number; 
    }
}