package com.oreillyauto.java.week1.day03;

public class Foo {

	public Foo() {
	}

	public static void main(String[] args) {
		System.out.println(Bar.x);
	}
}

class Bar {
	final static int x = 2;
}